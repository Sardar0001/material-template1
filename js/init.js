(function($) {
	$(function() {

		//initialize sidenav for mobile devices
		$('.button-collapse').sideNav();

		//back to top button scrolling animation
		$('#back-to-top').click(function() {
			$("html, body").animate({
				scrollTop : 0
			}, 1200, "swing");
		});	

		//initalize scrollspy
		$('.scrollspy').scrollSpy();				
	});
	// end of document ready
	$(window).load(function(){
		$('.preloader-wrapper').hide();
		$('.preloader').hide();
	})
})(jQuery);
// end of jQuery name space