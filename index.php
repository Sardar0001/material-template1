<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="theme-color" content="#EFB807">
		<title>Site title | Site Subtitle</title>
		
		<!-- Disable tap highlight on IE -->
	    <meta name="msapplication-tap-highlight" content="no">		   
	
	    <!-- Add to homescreen for Chrome on Android -->
	    <meta name="mobile-web-app-capable" content="yes">
	    <meta name="application-name" content="Web Starter Kit">
	    <link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png">
	
	    <!-- Add to homescreen for Safari on iOS -->
	    <meta name="apple-mobile-web-app-capable" content="yes">
	    <meta name="apple-mobile-web-app-status-bar-style" content="black">
	    <meta name="apple-mobile-web-app-title" content="Web Starter Kit">
	    <link rel="apple-touch-icon" href="images/touch/apple-touch-icon.png">
	
	    <!-- Tile icon for Win8 (144x144 + tile color) -->
	    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
	    <meta name="msapplication-TileColor" content="#3372DF">
		    
		
		<link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
		<link href="css/jquery.bxslider.css" type="text/css" rel="stylesheet" />
		<link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
		<!-- Google Material Design Fonts -->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	</head>
	<body id="body" class="scrollspy">		  
  		<div class="preloader">
	  		<div class="preloader-wrapper big active" style="margin: 0 auto; display: block;">
				<div class="spinner-layer spinner-blue-only">
					<div class="circle-clipper left">
						<div class="circle"></div>
				  	</div>
				  	<div class="gap-patch">
						<div class="circle"></div>
				  	</div>
				  	<div class="circle-clipper right">
						<div class="circle"></div>
				  	</div>
				</div>
			</div>
		</div>

		<!-- Nav bar -->
		<div class="navbar-fixed">
			<nav id="top-nav" role="navigation">										
				<div class="nav-wrapper">
					<a id="logo-container" href="#" class="brand-logo">Logo</a>
					<!-- Desktop Nav -->
					<ul class="center hide-on-med-and-down">
						<li>
							<a href="#body"><i class="material-icons left">dashboard</i> Home</a>
						</li>
						<li>
							<a href="#Services"><i class="material-icons left">list</i> Services</a>
						</li>
						<li>
							<a href="#"><i class="material-icons left">group_work</i> Partners</a>
						</li>
						<li>
							<a href="#Contact-us"><i class="material-icons left">message</i> Contact</a>
						</li>
					</ul>
					<!-- Mobile Nav -->
					<ul id="nav-mobile" class="side-nav">
						<li>
							<a href="#body"><i class="material-icons">dashboard</i> Home</a>
						</li>
						<li>
							<a href="#Services"><i class="material-icons">list</i> Services</a>
						</li>
						<li>
							<a href="#"><i class="material-icons">group_work</i> Partners</a>
						</li>
						<li>
							<a href="#Contact-us"><i class="material-icons">message</i> Contact</a>
						</li>
					</ul>
					<a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
				</div>
			</nav>
		</div>
		<!-- END of Nav bar -->

		<section id="index-banner">
			<div class="container">
				
    <br><br>
      <h1 class="header center orange-text">Starter Template</h1>
      <div class="row center">
        <h5 class="header col s12 light">A modern responsive front-end framework based on Material Design</h5>
      </div>
      <div class="row center">
        <a href="#" id="download-button" class="btn-large waves-effect waves-light orange">Get Started</a>
      </div>
      <br><br>

			</div>
		</section>

		<div class="container container-90 scrollspy" id="Services">
			 <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">flash_on</i></h2>
            <h5 class="center">Speeds up development</h5>

            <p class="light">We did most of the heavy lifting for you to provide a default stylings that incorporate our custom components. Additionally, we refined animations and transitions to provide a smoother experience for developers.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">group</i></h2>
            <h5 class="center">User Experience Focused</h5>

            <p class="light">By utilizing elements and principles of Material Design, we were able to create a framework that incorporates components and animations that provide more feedback to users. Additionally, a single underlying responsive system across all platforms allow for a more unified user experience.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">settings</i></h2>
            <h5 class="center">Easy to work with</h5>

            <p class="light">We have provided detailed documentation as well as specific code examples to help new users get started. We are also always open to feedback and can answer any questions a user may have about Materialize.</p>
          </div>
        </div>
      </div>

    </div>
		</div> <!-- END of Services Section -->



		<!-- Contact Us form Section -->
		<section id="Contact-us" class="container container-90 scrollspy">
			<h1 class="header center black-text"><i class="material-icons">message</i> Contact</h1>
			<div class="row">
				<form class="col s12" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
					<div class="row">
						<div class="input-field col s6">
							<input id="first_name" type="text" name="first_name" value="<?=$_POST['first_name']; ?>" class="<?=$validationState['fname']; ?>" required="">
							<label for="first_name">First Name (Required)</label>
							<span><?=$arr_errors['fname'] ?></span>
						</div>
						<div class="input-field col s6">
							<input id="last_name" name="last_name" type="text" value="<?=$_POST['last_name']; ?>" class="<?=$validationState['lname'] ?>" required="">
							<label for="last_name">Last Name (Required)</label>
							<span><?=$arr_errors['lname'] ?></span>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<input id="company" type="text" value="<?=$_POST['company']; ?>" name="company">
							<label for="company">Company (Optional)</label>
							<span><?=$arr_errors['company'] ?></span>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<input id="email" type="email" name="email" value="<?=$_POST['email']; ?>" class="<?=$validationState['email'] ?>" required>
							<label for="email">Email (Required)</label>
							<span><?=$arr_errors['email'] ?></span>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<textarea id="message" name="message" class="materialize-textarea <?=$validationState['message'] ?>" required><?=$_POST['message']; ?></textarea>
							<label for="message">Message (Required)</label>
							<span><?=$arr_errors['message'] ?></span>
						</div>
					</div>
					<button class="btn waves-effect waves-light" type="submit" name="submit">
						Send Message <i class="material-icons right">send</i>
					</button>
				</form>
			</div>
		</section>
		<!-- END of Contact Us Section -->

		<!-- Page Footer -->
		<footer class="page-footer">
			<div class="container container-90">
				<div class="col">
					<a href="#" class="right orange-text tooltipped" id="back-to-top" data-position="bottom" data-delay="50" data-tooltip="Back to Top"><i style="font-size: 42px;" class="material-icons">publish</i></a>
				</div>
				<div class="row">
					<div class="col l4 m10 s10 footer-logo">
						<a id="logo-container" href="#" class="brand-logo">Logo</a>
					</div>
					<div class="col l2 m5 s5">
						<ul>
							<li><a class="white-text" href="#!">Link 1</a></li>
				            <li><a class="white-text" href="#!">Link 2</a></li>
				            <li><a class="white-text" href="#!">Link 3</a></li>
				            <li><a class="white-text" href="#!">Link 4</a></li>
							</li>
						</ul>
					</div>
					<div class="col l4 m7 s7">
						<ul>
							<li><a class="white-text" href="#!">Link 1</a></li>
				            <li><a class="white-text" href="#!">Link 2</a></li>
				            <li><a class="white-text" href="#!">Link 3</a></li>
				            <li><a class="white-text" href="#!">Link 4</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="footer-copyright center">
				<div class="container container-90 center">
					Company | 123 street name | City Province Postal Code
					<a href="http://materializecss.com/" class="right orange-text" target="_blank"><strong>Powered by Materialize</strong></a>
				</div>
			</div>
		</footer>
		<!-- END of Page Footer -->

		<!-- Place Scripts in the end to improve performance -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="js/materialize.js"></script>		
		<script src="js/init.js"></script>		
		
		<?php
		require_once "helpers/validation_functions.php";	//all the validation functions are defined here
		
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$fname = test_input($_POST["first_name"]);
			$lname = test_input($_POST["last_name"]);
			$company = test_input($_POST["company"]);
			$email = test_input($_POST["email"]);
			$message = test_input($_POST["message"]);
		
			$arr_errors = array();			//array to hold error messages
			$validationState = array();		//array to hold validation class "invalid" for color coding input fields with error
			
			//first name validation
			if (!isValidName($fname)) {
				$arr_errors['fname'] = "First Name is Missing or Invalid";
				$validationState['fname'] = "invalid";
			}
			
			//last name validation
			if (!isValidName($lname)) {
				$arr_errors['lname'] = "Last Name is Missing or Invalid";
				$validationState['lname'] = "invalid";
			}
			
			//email validation
			if (!isValidEmail($email)) {
				$arr_errors['email'] = "Email is Missing or Invalid";
				$validationState['email'] = "invalid";
			}
			
			//message validation
			if (!isNotBlank($message)) {
				$arr_errors['message'] = "Message is Missing";
				$validationState['message'] = "invalid";
			}
			
			//if there is no error in the input fields, send the email
			if (empty($arr_errors)) {
				
				$to = "sukhdeepsaini12@gmail.com";	//emails will be sent to this address
				
				$subject = "Email from Templates website";
				$senderName = $fname . " " . $lname;
				$body = "<strong>Name</strong>: " . $senderName . "<br>";
				if ($company) {
					$body .= "<strong>Company Name</strong> : " . $company . "<br>";
				}
				$body .= "<strong>Email</strong> : " . $email . "<br>";
				$body .= "<strong>Message</strong> : " . $message . "<br>";
				
				$bodyNoHTML = "Name : " . $senderName . " Company Name : " .$company ." Email : " .$email." Message : " .$message;
				
				//sendNow($to, $email, $body, $bodyNoHTML, $subject,$senderName);
				
				/* Alternate version Using php's mail function */
				$headers = "From: {$email}\r\n";
				$headers .= "Reply-To: {$email}\r\n";
				$headers .= "MIME-Version: 1.0\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\n";
				
				mail($to, $subject, $body, $headers);
				?>	
				<script type="text/javascript">
					Materialize.toast('<i class="material-icons">done</i>Thank you, Your Message has been received', 3000, 'rounded');
				</script>
				<?php
			}
		}			
		?>
	</body>
</html>
